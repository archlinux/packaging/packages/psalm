# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>

pkgname=psalm
pkgver=6.8.9
pkgrel=1
pkgdesc='A static analysis tool for finding errors in PHP applications'
url="https://psalm.dev/"
arch=('any')
license=('MIT')
depends=('php')
makedepends=('composer' 'git')
source=("git+https://github.com/vimeo/psalm.git#tag=${pkgver}"
        "composer.lock")
b2sums=('6d18f6b80770beef111f706784d93aab4cace12208b3ad3dcea103bb7da2b20012d56d07e88607297d1cc121560d15b7f33de1ff244c18e7a921f4fb3e78794a'
        '59deed6987643977644c9c117d645cd1b4242f62bfddb5d8d1b546a6f0c511b7a624c2a325c3af9ab62be93472818744c30935e26d8be3c3a92cf53cbd884c66')

prepare() {
  cd ${pkgname}
  cp ../composer.lock .
  composer install
  composer bin box install
  php bin/ci/improve_class_alias.php
  php -r 'require "vendor/autoload.php"; Psalm\Internal\VersionUtils::dump();'
}

updlockfiles() {
  cd ${pkgname}
  rm -f composer.lock
  composer update --no-install
  cp composer.lock "${outdir}/"
}

build() {
  cd ${pkgname}
  vendor/bin/box compile
}

check() {
  rm -rf testing/
  mkdir -p testing/src
  cd testing/

  composer init -n --name test/test
  composer update
  composer install
  ../${pkgname}/build/psalm.phar --init
  ../${pkgname}/build/psalm.phar

  cat > src/index.php <<EOF
<?php echo(phpversion());
EOF
  ../${pkgname}/build/psalm.phar

  cat > src/index.php <<EOF
<?php echo(this_is_invalid_to_provoke_an_error());
EOF
  if ../${pkgname}/build/psalm.phar; then
    echo 'did not detect invalid php as invalid'
    exit 1
  fi
}

package() {
  cd ${pkgname}
  install -Dm 755 ./build/psalm.phar "${pkgdir}/usr/bin/psalm"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
